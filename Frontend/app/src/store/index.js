import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    enfoqueModelo:'TF-IDF_Lexicon',
    algoritmoML:'SVM',
    corpus:'CU',
    language:'es'
  },
  mutations: {
    actualizarEnfoque (state, valor) {
      state.enfoqueModelo = valor
    },
    actualizarAlgortimoML (state, valor) {
      state.algoritmoML = valor
    },
    actualizarCorpus (state, valor) {
      state.corpus = valor
    },
    actualizarLanguage (state, valor) {
      state.language = valor
    }
  },
  actions: {
  },
  modules: {
  }
})
