export default{
    data() {
        return {
            en:{
                TituloBar: "Hybrid models based on Lexicons and Machine Learning for the detection of aggressiveness on Spanish texts.",
                MenuClasificarTweetMan: "Classify Tweets manually",
                MenuVerresultados: "View experiment results",
                MenuVerTweetClas: "View classified tweets",
                MenuEvaluaModelo: "Evaluate models",
                MenuAyuda: "Help",
                CodigoFuenteWebBar: "Web source code",
                CodigoFuenteExpbBar: "Experiments source code",
                GrupoFooter:"Research group",
                DesarrolladoFooter:"Developed by",
                
                TituloElegirModelo: "Choose model",
                LabelEnfoques: "Approaches",
                LabelAlgoritmos: "Algorithms",

                TituloClasificarTweets: "Classification of Tweets",
                LabelBuscarTweets: "Search by:",
                LabelIngresarPalabras:"Enter keywords",
                LabelIngresarUsuario:"Enter username",
                CampoRequerido: "This field is required",
                LabelNumeroTweets: "Number of Tweets",
                BtnClasificarTweets: "Classify Tweets",
                
                TituloClasificarFrases: "Classification of sentences",
                LabelInputFrases:"Enter sentences (separated by enter)",
                ValidarFrasesVacio: "Enter one or more sentences",
                BtnClasificarFrases: "Classify sentences",
                BtnLimpiar:"Clean",

                TituloClasificacionManual:"Manual classification of tweets",
                DescripcionCM: "Feedback for the selected model will be saved in a database for future work.",

                TituloVerResultados:"Results of the experimental stage",
                TituloAyuda:"Help: Meaning of the acronyms used",
                BajadaAyuda_1: "The names of the models are as follows.",
                BajadaAyuda_2: "[Name approach]_[ML Classifier Name]_[Name Corpus]",
                AyudaTr_1: "Acronym",
                AyudaTr_2: "Meaning",
                TituloDescargarCorpus: "Download corpus used",

                TituloVerTweets: "Classified tweets saved in the database",
                TituloEvaluarModelos: "Evaluation of models",
                BajadaEvaluarModelos: "This may take several minutes depending on the number of instances of the test corpus.",
                DescargarPlantilla: "Download template",
                LabelCargarCorpus: "Upload file",
                BtnEvaluarModelo: "Evaluate the selected model",
                TituloResultadosEv: "Evaluation result",
                NombreMetricaEv: "Name metric",
                ResultadoEv: "Result",

                TortaAgresivo: "Aggressive",
                TortaNoAgresivo: "non-aggressive",

                BuscarTabla: "Search",
                BtnGuardarExcel: "Save to an excel file",
                BtnGuardarClasificacionMan: "Save manual classification"


            },
        };
    },
};