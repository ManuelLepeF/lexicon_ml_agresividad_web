export default {
    data() {
        return {
            es: {
                TituloBar: "Modelos híbridos basados en Lexicones y Machine Learning para la detección de agresividad sobre textos en idioma Español",
                MenuClasificarTweetMan: "Clasificar Tweets manualmente",
                MenuVerresultados: "Ver resultados experimentos",
                MenuVerTweetClas: "Ver tweets clasificados",
                MenuEvaluaModelo: "Evaluar modelos",
                MenuAyuda: "Ayuda",
                CodigoFuenteWebBar: "Código fuente web",
                CodigoFuenteExpbBar: "Código fuente experimentos",
                GrupoFooter:"Grupo de investigación",
                DesarrolladoFooter:"Desarrollado por",
                
                TituloElegirModelo: "Seleccione el modelo a usar",
                LabelEnfoques: "Enfoques",
                LabelAlgoritmos: "Algoritmos",

                TituloClasificarTweets: "Clasificar Tweets",
                LabelBuscarTweets: "Buscar Tweets por:",
                LabelIngresarPalabras:"Ingresar palabras claves",
                LabelIngresarUsuario:"Ingresar nombre de usuario",
                CampoRequerido: "Este campo es requerido",
                LabelNumeroTweets: "Número de Tweets",
                BtnClasificarTweets: "Clasificar tweets",

                TituloClasificarFrases: "Clasificar Frases",
                LabelInputFrases:"Ingresar frases (separadas por enter)",
                ValidarFrasesVacio: "Ingrese una o mas frases",
                BtnClasificarFrases: "Clasificar Frases",
                BtnLimpiar:"Limpiar",

                TituloClasificacionManual:"Clasificar Tweets manualmente",
                DescripcionCM: "Retroalimentación para el modelo seleccionado, se guardará en una base de datos para futuros trabajos. ",

                TituloVerResultados:"Resultados de la etapa experimental",
                TituloAyuda:"Ayuda: Significados de las siglas utilizadas",
                BajadaAyuda_1: "Los nombre de los modelos estan conformados se la siguiente forma",
                BajadaAyuda_2: "[Nombre enfoque]_[Nombre Clasificador de ML]_[Nombre Corpus]",
                AyudaTr_1: "Sigla",
                AyudaTr_2: "Significado",
                TituloDescargarCorpus: "Descargar corpus utilizados",

                TituloVerTweets: "Tweets clasificados guardados en la base de datos",
                TituloEvaluarModelos: "Evaluar modelos",
                BajadaEvaluarModelos: "Esto puede tardar varios minutos dependiendo del número de instancias del corpus de prueba.",
                DescargarPlantilla: "Descargar plantilla",
                LabelCargarCorpus: "Cargar archivo csv con corpus de prueba",
                BtnEvaluarModelo: "Evaluar el modelo seleccionado",
                TituloResultadosEv: "Resultados de la evaluación",
                NombreMetricaEv: "Nombre métrica",
                ResultadoEv: "Resultado",

                TortaAgresivo: "Agresivo",
                TortaNoAgresivo: "No agresivo",
                
                BuscarTabla: "Buscar",
                BtnGuardarExcel: "Guardar en un archivo excel",
                BtnGuardarClasificacionMan: "Guardar clasificación manual"

                

            },
        };
    },
};