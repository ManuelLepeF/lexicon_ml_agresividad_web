# Lexicon_ML_Agresividad_Web

Web that implements hybrid models based on Lexicons and Machine Learning for the detection of aggressiveness on Spanish texts.

# Requirements

1. Docker
2. Docker-compose

# Steps to deploy the app

1. Edit environment variables (Twitter credentials) and run on the root:

    `docker-compose up -d`
    
2. Insert results in the database using the l [sql file](Backend/sql_Resultados.sql) and pgAdmin4 (ip:5555) with the credentials defined in the [file](docker-compose.yml) (To connect to the DB you must use the host: lexicon_ml_agresividad_web_db_1)

# Screenshot Web

![Home](Imagenes_Web/web_inicio.png)

![Classification of tweets by keywords](Imagenes_Web/web_tweet_palabrasClaves.png)

![Classification of tweets a username](Imagenes_Web/web_tweet_usuario.png)

![Classification of sentences](Imagenes_Web/web_frases.png)

![Manual classification of tweets](Imagenes_Web/web_clasificar_manual.png)

![Classified tweets saved in the database ](Imagenes_Web/web_manual_guardados.png)

![View experiments results](Imagenes_Web/web_resultados.png)

![Evaluate model](Imagenes_Web/web_evaluar.png)

# To be considered
The DB data is stored in the /Backend/db_data defined in this [file](docker-compose.yml).

To understand the models implemented please read: [Master Thesis](http://mcc.ubiobio.cl/docs/tesis/manuel_lepe-tesis(manuellepe).pdf)



