------------------------------------------------------------- TF-IDF -------------------------------------------------------

-- Support Vector Machine --
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('TF-IDF_SVM_CU', 'TF-IDF', 'Support Vector Machine', 'Chileno', 0.8690, 0.8671, 0.8715, 0.8690, 'Enfoque TF-IDF con clasificador SVM entrenado y testeado en el corpus Chileno');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure  , precision, recall , descripcion)
VALUES ('TF-IDF_SVM_MX', 'TF-IDF', 'Support Vector Machine', 'Mexicano', 0.8281, 0.8225, 0.8234, 0.8281, 'Enfoque TF-IDF con clasificador SVM entrenado y testeado en el corpus Mexicano');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('TF-IDF_SVM_CUMX', 'TF-IDF', 'Support Vector Machine', 'Chileno+Mexicano', 0.8473,0.8424 , 0.8459, 0.8473, 'Enfoque TF-IDF con clasificador SVM entrenado y testeado en el corpus Chileno+Mexicano');

-- Naive Bayes --
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('TF-IDF_NB_CU', 'TF-IDF', 'Naive Bayes', 'Chileno',0.7530 ,0.7514 , 0.7511, 0.7530, 'Enfoque TF-IDF con clasificador NB entrenado y testeado en el corpus Chileno');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('TF-IDF_NB_MX', 'TF-IDF', 'Naive Bayes', 'Mexicano', 0.7522, 0.7336,0.7372 ,0.7522 , 'Enfoque TF-IDF con clasificador NB entrenado y testeado en el corpus Mexicano');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('TF-IDF_NB_CUMX', 'TF-IDF', 'Naive Bayes', 'Chileno+Mexicano', 0.75, 0.7351, 0.7397, 0.75, 'Enfoque TF-IDF con clasificador NB entrenado y testeado en el corpus Chileno+Mexicano');

-- Random Forest --
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('TF-IDF_RF_CU', 'TF-IDF', 'Random Forest', 'Chileno',0.8717 ,0.8701 , 0.8736,0.8717 , 'Enfoque TF-IDF con clasificador RF entrenado y testeado en el corpus Chileno');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure, precision, recall , descripcion)
VALUES ('TF-IDF_RF_MX', 'TF-IDF', 'Random Forest', 'Mexicano', 0.8204, 0.8069, 0.8203, 0.8204 , 'Enfoque TF-IDF con clasificador RF entrenado y testeado en el corpus Mexicano');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('TF-IDF_RF_CUMX', 'TF-IDF', 'Random Forest', 'Chileno+Mexicano', 0.8418, 0.8334, 0.8450, 0.8418, 'Enfoque TF-IDF con clasificador RF entrenado y testeado en el corpus Chileno+Mexicano');

------------------------------------------------------------- LEXICON -------------------------------------------------------

-- Support Vector Machine --
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('Lexicon_SVM_CU', 'Lexicon', 'Support Vector Machine', 'Chileno', 0.8556, 0.8535, 0.8571 , 0.8556, 'Enfoque Lexicon con clasificador SVM entrenado y testeado en el corpus Chileno');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure  , precision, recall , descripcion)
VALUES ('Lexicon_SVM_MX', 'Lexicon', 'Support Vector Machine', 'Mexicano', 0.6977,0.5735 , 0.4868,0.6977 , 'Enfoque Lexicon con clasificador SVM entrenado y testeado en el corpus Mexicano');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('Lexicon_SVM_CUMX', 'Lexicon', 'Support Vector Machine', 'Chileno+Mexicano', 0.7154, 0.6528,0.7251 , 0.7154, 'Enfoque Lexicon con clasificador SVM entrenado y testeado en el corpus Chileno+Mexicano');

-- Naive Bayes --
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('Lexicon_NB_CU', 'Lexicon', 'Naive Bayes', 'Chileno', 0.8367, 0.8321, 0.8438, 0.83670, 'Enfoque Lexicon con clasificador NB entrenado y testeado en el corpus Chileno');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('Lexicon_NB_MX', 'Lexicon', 'Naive Bayes', 'Mexicano', 0.6822, 0.6408,0.6377 ,0.6822 , 'Enfoque Lexicon con clasificador NB entrenado y testeado en el corpus Mexicano');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('Lexicon_NB_CUMX', 'Lexicon', 'Naive Bayes', 'Chileno+Mexicano', 0.7113, 0.6865, 0.6925,0.7113 , 'Enfoque Lexicon con clasificador NB entrenado y testeado en el corpus Chileno+Mexicano');

-- Random Forest --
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('Lexicon_RF_CU', 'Lexicon', 'Random Forest', 'Chileno', 0.8636, 0.8627,0.8635 ,0.8636 , 'Enfoque Lexicon con clasificador RF entrenado y testeado en el corpus Chileno');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure, precision, recall , descripcion)
VALUES ('Lexicon_RF_MX', 'Lexicon', 'Random Forest', 'Mexicano', 0.7, 0.6347, 0.6572, 0.7 , 'Enfoque Lexicon con clasificador RF entrenado y testeado en el corpus Mexicano');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('Lexicon_RF_CUMX', 'Lexicon', 'Random Forest', 'Chileno+Mexicano',0.7211 ,0.70162 , 0.7053, 0.7211, 'Enfoque Lexicon con clasificador RF entrenado y testeado en el corpus Chileno+Mexicano');


------------------------------------------------------------- TF-IDF_LEXICON_ -------------------------------------------------------

-- Support Vector Machine --
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('TF-IDF_Lexicon_SVM_CU', 'TF-IDF_Lexicon', 'Support Vector Machine', 'Chileno', 0.8663, 0.8648, 0.8674 , 0.8663, 'Enfoque TF-IDF_Lexicon con clasificador SVM entrenado y testeado en el corpus Chileno');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure  , precision, recall , descripcion)
VALUES ('TF-IDF_Lexicon_SVM_MX', 'TF-IDF_Lexicon', 'Support Vector Machine', 'Mexicano',0.8395 , 0.8330, 0.8363,0.8395 , 'Enfoque TF-IDF_Lexicon con clasificador SVM entrenado y testeado en el corpus Mexicano');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('TF-IDF_Lexicon_SVM_CUMX', 'TF-IDF_Lexicon', 'Support Vector Machine', 'Chileno+Mexicano', 0.8439, 0.8372, 0.8443, 0.8439, 'Enfoque TF-IDF_Lexicon con clasificador SVM entrenado y testeado en el corpus Chileno+Mexicano');

-- Naive Bayes --
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('TF-IDF_Lexicon_NB_CU', 'TF-IDF_Lexicon', 'Naive Bayes', 'Chileno', 0.7813, 0.7829,0.7895 ,0.7813 , 'Enfoque TF-IDF_Lexicon con clasificador NB entrenado y testeado en el corpus Chileno');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('TF-IDF_Lexicon_NB_MX', 'TF-IDF_Lexicon', 'Naive Bayes', 'Mexicano', 0.7486,0.7420 , 0.7391,0.7486 , 'Enfoque TF-IDF_Lexicon con clasificador NB entrenado y testeado en el corpus Mexicano');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('TF-IDF_Lexicon_NB_CUMX', 'TF-IDF_Lexicon', 'Naive Bayes', 'Chileno+Mexicano',0.7558 , 0.7543, 0.7531, 0.7558, 'Enfoque TF-IDF_Lexicon con clasificador NB entrenado y testeado en el corpus Chileno+Mexicano');

-- Random Forest --
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('TF-IDF_Lexicon_RF_CU', 'TF-IDF_Lexicon', 'Random Forest', 'Chileno', 0.8852, 0.8839, 0.8868, 0.8852, 'Enfoque TF-IDF_Lexicon con clasificador RF entrenado y testeado en el corpus Chileno');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure, precision, recall , descripcion)
VALUES ('TF-IDF_Lexicon_RF_MX', 'TF-IDF_Lexicon', 'Random Forest', 'Mexicano',0.8122 , 0.7960, 0.8131, 0.8122, 'Enfoque TF-IDF_Lexicon con clasificador RF entrenado y testeado en el corpus Mexicano');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('TF-IDF_Lexicon_RF_CUMX', 'TF-IDF_Lexicon', 'Random Forest', 'Chileno+Mexicano', 0.8340,0.8231 , 0.8400, 0.8340, 'Enfoque TF-IDF_Lexicon con clasificador RF entrenado y testeado en el corpus Chileno+Mexicano');


------------------------------------------------------------- WordEmbedding -------------------------------------------------------
-- Support Vector Machine --
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('WE_SVM_CU', 'WE', 'Support Vector Machine', 'Chileno',0.8569 ,0.8547 , 0.8590, 0.8569, 'Enfoque WE con clasificador SVM entrenado y testeado en el corpus Chileno');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure  , precision, recall , descripcion)
VALUES ('WE_SVM_MX', 'WE', 'Support Vector Machine', 'Mexicano', 0.7972,0.7831 , 0.7913,0.7972 , 'Enfoque WE con clasificador SVM entrenado y testeado en el corpus Mexicano');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('WE_SVM_CUMX', 'WE', 'Support Vector Machine', 'Chileno+Mexicano', 0.8021,0.7900 ,0.8008 , 0.8021, 'Enfoque WE con clasificador SVM entrenado y testeado en el corpus Chileno+Mexicano');

-- Naive Bayes --
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('WE_NB_CU', 'WE', 'Naive Bayes', 'Chileno',0.8259 ,0.8253 ,0.8252 , 0.8259, 'Enfoque WE con clasificador NB entrenado y testeado en el corpus Chileno');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('WE_NB_MX', 'WE', 'Naive Bayes', 'Mexicano', 0.745, 0.7504, 0.7605, 0.745, 'Enfoque WE con clasificador NB entrenado y testeado en el corpus Mexicano');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('WE_NB_CUMX', 'WE', 'Naive Bayes', 'Chileno+Mexicano', 0.7599,0.7633 , 0.7694 ,0.7599 , 'Enfoque WE con clasificador NB entrenado y testeado en el corpus Chileno+Mexicano');

-- Random Forest --
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('WE_RF_CU', 'WE', 'Random Forest', 'Chileno',0.8218 ,0.8170 , 0.8275, 0.8218, 'Enfoque WE con clasificador RF entrenado y testeado en el corpus Chileno');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure, precision, recall , descripcion)
VALUES ('WE_RF_MX', 'WE', 'Random Forest', 'Mexicano', 0.7713,0.7296 ,0.7899 , 0.7713, 'Enfoque WE con clasificador RF entrenado y testeado en el corpus Mexicano');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('WE_RF_CUMX', 'WE', 'Random Forest', 'Chileno+Mexicano', 0.7616, 0.7252, 0.7798 , 0.7616, 'Enfoque WE con clasificador RF entrenado y testeado en el corpus Chileno+Mexicano');

------------------------------------------------------------- WE_Lexicon -------------------------------------------------------
-- Support Vector Machine --
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('WE_Lexicon_SVM_CU', 'WE_Lexicon', 'Support Vector Machine', 'Chileno', 0.8920,0.8908 , 0.8936, 0.8920, 'Enfoque WE_Lexicon con clasificador SVM entrenado y testeado en el corpus Chileno');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure  , precision, recall , descripcion)
VALUES ('WE_Lexicon_SVM_MX', 'WE_Lexicon', 'Support Vector Machine', 'Mexicano', 0.8027,0.7874 ,0.7991 , 0.8027, 'Enfoque WE_Lexicon con clasificador SVM entrenado y testeado en el corpus Mexicano');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('WE_Lexicon_SVM_CUMX', 'WE_Lexicon', 'Support Vector Machine', 'Chileno+Mexicano', 0.8184, 0.8086,0.8181 , 0.8184, 'Enfoque WE_Lexicon con clasificador SVM entrenado y testeado en el corpus Chileno+Mexicano');

-- Naive Bayes --
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('WE_Lexicon_NB_CU', 'WE_Lexicon', 'Naive Bayes', 'Chileno',0.8502 , 0.8495, 0.8496, 0.8502, 'Enfoque WE_Lexicon con clasificador NB entrenado y testeado en el corpus Chileno');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('WE_Lexicon_NB_MX', 'WE_Lexicon', 'Naive Bayes', 'Mexicano',0.7495 , 0.7551, 0.7658,0.7495 , 'Enfoque WE_Lexicon con clasificador NB entrenado y testeado en el corpus Mexicano');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('WE_Lexicon_NB_CUMX', 'WE_Lexicon', 'Naive Bayes', 'Chileno+Mexicano', 0.7664,0.7696 , 0.7756, 0.7664, 'Enfoque WE_Lexicon con clasificador NB entrenado y testeado en el corpus Chileno+Mexicano');

-- Random Forest --
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('WE_Lexicon_RF_CU', 'WE_Lexicon', 'Random Forest', 'Chileno', 0.8852, 0.8833, 0.8892, 0.8852, 'Enfoque WE_Lexicon con clasificador RF entrenado y testeado en el corpus Chileno');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure, precision, recall , descripcion)
VALUES ('WE_Lexicon_RF_MX', 'WE_Lexicon', 'Random Forest', 'Mexicano', 0.7590, 0.7107, 0.7760, 0.7590, 'Enfoque WE_Lexicon con clasificador RF entrenado y testeado en el corpus Mexicano');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('WE_Lexicon_RF_CUMX', 'WE_Lexicon', 'Random Forest', 'Chileno+Mexicano',0.7626 ,0.7257 ,0.7832 ,0.7626 , 'Enfoque WE_Lexicon con clasificador RF entrenado y testeado en el corpus Chileno+Mexicano');

------------------------------------------------------------- WE_Lexicon_TF-IDF -------------------------------------------------------
-- Support Vector Machine --
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('WE_Lexicon_TF-IDF_SVM_CU', 'WE_Lexicon_TF-IDF', 'Support Vector Machine', 'Chileno', 0.8744,0.8731 ,0.8755 , 0.8744, 'Enfoque WE_Lexicon_TF-IDF con clasificador SVM entrenado y testeado en el corpus Chileno');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure  , precision, recall , descripcion)
VALUES ('WE_Lexicon_TF-IDF_SVM_MX', 'WE_Lexicon_TF-IDF', 'Support Vector Machine', 'Mexicano',0.8431 , 0.8394, 0.8395,0.8431 , 'Enfoque WE_Lexicon_TF-IDF con clasificador SVM entrenado y testeado en el corpus Mexicano');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('WE_Lexicon_TF-IDF_SVM_CUMX', 'WE_Lexicon_TF-IDF', 'Support Vector Machine', 'Chileno+Mexicano', 0.8548, 0.8507, 0.8534, 0.8548, 'Enfoque WE_Lexicon_TF-IDF con clasificador SVM entrenado y testeado en el corpus Chileno+Mexicano');

-- Naive Bayes --
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('WE_Lexicon_TF-IDF_NB_CU', 'WE_Lexicon_TF-IDF', 'Naive Bayes', 'Chileno', 0.7827,0.7842 ,0.7906 , 0.7827, 'Enfoque WE_Lexicon_TF-IDF con clasificador NB entrenado y testeado en el corpus Chileno');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('WE_Lexicon_TF-IDF_NB_MX', 'WE_Lexicon_TF-IDF', 'Naive Bayes', 'Mexicano', 0.7486, 0.7420, 0.7391, 0.7486, 'Enfoque WE_Lexicon_TF-IDF con clasificador NB entrenado y testeado en el corpus Mexicano');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('WE_Lexicon_TF-IDF_NB_CUMX', 'WE_Lexicon_TF-IDF', 'Naive Bayes', 'Chileno+Mexicano', 0.7548, 0.7537, 0.7529, 0.7548, 'Enfoque WE_Lexicon_TF-IDF con clasificador NB entrenado y testeado en el corpus Chileno+Mexicano');

-- Random Forest --
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('WE_Lexicon_TF-IDF_RF_CU', 'WE_Lexicon_TF-IDF', 'Random Forest', 'Chileno',0.8542 , 0.8501, 0.8630, 0.8542, 'Enfoque WE_Lexicon_TF-IDF con clasificador RF entrenado y testeado en el corpus Chileno');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure, precision, recall , descripcion)
VALUES ('WE_Lexicon_TF-IDF_RF_MX', 'WE_Lexicon_TF-IDF', 'Random Forest', 'Mexicano',0.7590 , 0.7061,0.7874 ,0.7590 , 'Enfoque WE_Lexicon_TF-IDF con clasificador RF entrenado y testeado en el corpus Mexicano');

INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('WE_Lexicon_TF-IDF_RF_CUMX', 'WE_Lexicon_TF-IDF', 'Random Forest', 'Chileno+Mexicano',0.7528 , 0.7033, 0.7941, 0.7528, 'Enfoque WE_Lexicon_TF-IDF con clasificador RF entrenado y testeado en el corpus Chileno+Mexicano');


------------------------------------------------------------- TF-IDF_Lexicon_E_Clf-------------------------------------------------------
-- Chileno --
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('TF-IDF_Lexicon_E_Clf_CU', 'TF-IDF_Lexicon_E_Clf', 'SVM - NB - RF ', 'Chileno', 0.8839, 0.8828,0.8848 , 0.8839, 'Enfoque TF-IDF_Lexicon_E_Clf entrenado y testeado en el corpus Chileno');

-- Mexicano --
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure  , precision, recall , descripcion)
VALUES ('TF-IDF_Lexicon_E_Clf_MX', 'TF-IDF_Lexicon_E_Clf', 'SVM - NB - RF ', 'Mexicano', 0.8309, 0.8191, 0.8316,0.8309 , 'Enfoque TF-IDF_Lexicon_E_Clf entrenado y testeado en el corpus Mexicano');

-- Chileno + Mexicano--
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('TF-IDF_Lexicon_E_Clf_CUMX', 'TF-IDF_Lexicon_E_Clf', 'SVM - NB - RF ', 'Chileno+Mexicano', 0.8480, 0.8399, 0.8518,0.8480 , 'Enfoque TF-IDF_Lexicon_E_Clf entrenado y testeado en el corpus Chileno+Mexicano');

------------------------------------------------------------- TFIDF_Lexicon_E_SVM-------------------------------------------------------
-- Chileno --
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('TFIDF_Lexicon_E_SVM_CU', 'TFIDF_Lexicon_E_SVM', 'SVM', 'Chileno', 0.8731, 0.8716,0.8745 , 0.8731, 'Enfoque TFIDF_Lexicon_E_SVM entrenado y testeado en el corpus Chileno');

-- Mexicano --
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure  , precision, recall , descripcion)
VALUES ('TFIDF_Lexicon_E_SVM_MX', 'TFIDF_Lexicon_E_SVM', 'SVM', 'Mexicano',0.8386 , 0.8308, 0.8362, 0.8386, 'Enfoque TFIDF_Lexicon_E_SVM entrenado y testeado en el corpus Mexicano');

-- Chileno + Mexicano--
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('TFIDF_Lexicon_E_SVM_CUMX', 'TFIDF_Lexicon_E_SVM', 'SVM', 'Chileno+Mexicano', 0.8435, 0.8356, 0.8461,0.8435, 'Enfoque TFIDF_Lexicon_E_SVM entrenado y testeado en el corpus Chileno+Mexicano');



------------------------------------------------------------- WE_Lexicon_TF-IDF_E_SVM-------------------------------------------------------
-- Chileno --
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('WE_Lexicon_TF-IDF_E_SVM_CU', 'WE_Lexicon_TF-IDF_E_SVM', 'SVM', 'Chileno',0.8866 ,0.8851 , 0.8891, 0.8866,  'Enfoque WE_Lexicon_TF-IDF_E_SVM entrenado y testeado en el corpus Chileno');

-- Mexicano --
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure  , precision, recall , descripcion)
VALUES ('WE_Lexicon_TF-IDF_E_SVM_MX', 'WE_Lexicon_TF-IDF_E_SVM', 'SVM', 'Mexicano', 0.8022, 0.7879, 0.7977, 0.8022,  'Enfoque WE_Lexicon_TF-IDF_E_SVM entrenado y testeado en el corpus Mexicano');

-- Chileno + Mexicano--
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('WE_Lexicon_TF-IDF_E_SVM_CUMX', 'WE_Lexicon_TF-IDF_E_SVM', 'SVM', 'Chileno+Mexicano',0.8310 , 0.8219, 0.8325, 0.8310, 'Enfoque WE_Lexicon_TF-IDF_E_SVM entrenado y testeado en el corpus Chileno+Mexicano');

------------------------------------------------------------- Enfoques_E_SVM-------------------------------------------------------
-- Chileno --
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('Enfoques_E_SVM_CU', 'Enfoques_E_SVM', 'SVM', 'Chileno', 0.8825, 0.8804, 0.8873, 0.8825,'Enfoque Enfoques_E_SVM entrenado y testeado en el corpus Chileno');

-- Mexicano --
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure  , precision, recall , descripcion)
VALUES ('Enfoques_E_SVM_MX', 'Enfoques_E_SVM', 'SVM', 'Mexicano', 0.8081, 0.7868, 0.8157, 0.8081, 'Enfoque Enfoques_E_SVM entrenado y testeado en el corpus Mexicano');

-- Chileno + Mexicano--
INSERT INTO modelos (id_modelo, enfoque, algoritmo_ml, corpus, accuracy, f_measure , precision, recall , descripcion)
VALUES ('Enfoques_E_SVM_CUMX', 'Enfoques_E_SVM', 'SVM', 'Chileno+Mexicano',0.8289, 0.8146, 0.8415, 0.8289, 'Enfoque Enfoques_E_SVM entrenado y testeado en el corpus Chileno+Mexicano');
