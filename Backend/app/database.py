from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
import os 

os.getenv('USERNAME', 'test')

SQLALCHEMY_DATABASE_URL = "postgresql://%s:%s@%s/%s" % (os.getenv("POSTGRES_USER", "admin"),
                            os.getenv("POSTGRES_PASSWORD", "admin"),
                            os.getenv("POSTGRES_SERVER", "lexicon_ml_agresividad_web_db_1"),
                            os.getenv("POSTGRES_DB", "tesis_db"),)

engine = create_engine(SQLALCHEMY_DATABASE_URL)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()

def get_db():
    db = SessionLocal()
    try:
        yield db
    except:
        db.close()
