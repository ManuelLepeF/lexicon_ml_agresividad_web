# ! /usr/bin/env bash

# Run custom Python script before starting
python config_nltk.py

# Run alembic
alembic upgrade head