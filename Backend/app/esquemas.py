from pydantic import BaseModel
from enum import Enum
from typing import List, Optional

class insertarModelo(BaseModel):
    id_modelo : str
    enfoque : str
    algoritmo_ml : str
    corpus : str
    f_measure :  float
    accuracy : float
    precision : float
    recall : float
    descripcion : str

class insertarTweet(BaseModel):
    tweet : str
    clf_modelo : str
    clf_humano : str
    id_modelo : str


class modelo_entrenado(str, Enum):
    TFIDF = "TF-IDF"
    Lexicon = "Lexicon"
    TFIDF_Lexicon = "TF-IDF_Lexicon"
    WE = "WE"
    WE_Lexicon = "WE_Lexicon"
    WE_Lexicon_TFIDF = "WE_Lexicon_TF-IDF"
    TFIDF_Lexicon_E_Clf = "TF-IDF_Lexicon_E_Clf"
    TFIDF_Lexicon_E_SVM = "TF-IDF_Lexicon_E_SVM"
    WE_Lexicon_TFIDF_E_SVM = "WE_Lexicon_TF-IDF_E_SVM"
    Enfoques_E_SVM = "Enfoques_E_SVM"

class clasificadores(str, Enum):
    SVM = "SVM"
    NB = "NB"
    RF = "RF"
    NO = "-"

class corpus(str, Enum):
    CU = "CU"
    MX = "MX"
    CUMX = "CUMX"

class tipoTweet(str, Enum):
    palabrasClaves = "Palabras Claves"
    usuario = "Tweets usuario"

class clasificacionTweet(BaseModel):
    tweet : str
    clf_modelo : str

class tweetsClasificados(BaseModel):
    numero_agresivos: int
    numero_noAgresivos: int
    listaTweetClasificados : List[clasificacionTweet]

class modeloMultiplesTexto(BaseModel):
    listaTexto: List[str]
    modelo: modelo_entrenado
    clasificador: clasificadores
    corpus: corpus

