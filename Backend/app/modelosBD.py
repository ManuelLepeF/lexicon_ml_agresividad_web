from sqlalchemy import Integer, String, Float, ForeignKey
from sqlalchemy.sql.schema import Column
from database import Base

class modelos(Base):
    __tablename__ = 'modelos'

    id_modelo = Column(String, primary_key=True)
    enfoque = Column(String, nullable=False)
    algoritmo_ml = Column(String, nullable=False)
    corpus = Column(String, nullable=False)
    f_measure =  Column(Float, nullable=False)
    accuracy = Column(Float, nullable=False)
    precision = Column(Float, nullable=False)
    recall = Column(Float, nullable=False)
    descripcion = Column(String, nullable=False)


class tweets_clfs(Base):
    __tablename__ = 'tweets_clfs'

    id_tweet = Column(Integer, primary_key=True)
    tweet = Column(String, nullable=False)
    clf_modelo = Column(String, nullable=False)
    clf_humano = Column(String, nullable=False)
    id_modelo = Column(String, ForeignKey('modelos.id_modelo'))

    