"""init

Revision ID: eb0173ab3798
Revises: 
Create Date: 2020-12-23 13:31:03.965743

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = 'eb0173ab3798'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'modelos',
        sa.Column('id_modelo', sa.String, primary_key=True),
        sa.Column('enfoque', sa.String, nullable=False),
        sa.Column('algoritmo_ml', sa.String,nullable=False),
        sa.Column('corpus', sa.String,nullable=False),
        sa.Column('f_measure', sa.Float,nullable=False),
        sa.Column('accuracy', sa.Float,nullable=False),
        sa.Column('precision', sa.Float,nullable=False),
        sa.Column('recall', sa.Float,nullable=False),
        sa.Column('descripcion', sa.String, nullable=False)

    )
    op.create_table(
        'tweets_clfs',
        sa.Column('id_tweet', sa.Integer, primary_key=True),
        sa.Column('tweet', sa.String, nullable=False),
        sa.Column('clf_modelo', sa.String, nullable=False),
        sa.Column('clf_humano', sa.String, nullable=False),
        sa.Column('id_modelo', sa.String, sa.ForeignKey('modelos.id_modelo')),
    )


def downgrade():
    op.drop_table('modelos')
    op.drop_table('tweets_clfs')
