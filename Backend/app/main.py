from fastapi import FastAPI, Depends, Query
from sqlalchemy.orm import Session
from esquemas import *
from database import get_db
from modelosBD import modelos, tweets_clfs
from typing import List
import tweepy
import CredencialesTwitter
from joblib import load
from fastapi.middleware.cors import CORSMiddleware
from fastapi.responses import FileResponse
from fastapi import FastAPI, File, UploadFile, Form
import pandas as pd
import numpy as np
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score

app = FastAPI()

origins = [
    "http://localhost:8080",
    "http://localhost"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


# PREDICE UN TEXTO
@app.get("/predecir_texto/{texto}")
def predecir_texto(texto: str, modelo: modelo_entrenado,clasificador:clasificadores, corpus:corpus):
    if(clasificador == '-'):
        pipe = load('Modelos_entrenados/'+modelo+'_'+corpus+'.joblib')
    else:
        pipe = load('Modelos_entrenados/'+modelo+'_'+clasificador+'_'+corpus+'.joblib')
    predict = pipe.predict([texto])
    return {"Modelo":modelo, "Clasificador":clasificador, "Corpus":corpus, "Clasificacion": predict[0]}

# EXTRAE Y PREDICE TWEETS
@app.get("/predecir_tweet/", response_model=tweetsClasificados)
def predecir_tweet(cantidadTweet: int, 
    tipoTweet: tipoTweet, 
    textoPalabrasClaves: str, 
    modelo: modelo_entrenado,
    clasificador:clasificadores, 
    corpus:corpus):
    # CARGA MODELO
    if(clasificador == '-'):
        pipe = load('Modelos_entrenados/'+modelo+'_'+corpus+'.joblib')
    else:
        pipe = load('Modelos_entrenados/'+modelo+'_'+clasificador+'_'+corpus+'.joblib')
    # SE CONECTA A LA API
    auth = tweepy.OAuthHandler(CredencialesTwitter.CONSUMER_KEY, CredencialesTwitter.CONSUMER_SECRET)
    auth.set_access_token(CredencialesTwitter.ACCESS_TOKEN,CredencialesTwitter.ACCESS_TOKEN_SECRET)
    api = tweepy.API(auth,wait_on_rate_limit=True, wait_on_rate_limit_notify=True)
    if (tipoTweet == "Palabras Claves"):
        keySearch = textoPalabrasClaves+ "-filter:retweets"    
    else:
        keySearch = "from:"+textoPalabrasClaves+ " -filter:retweets"
    numeroAgresivos = 0
    numeroNoAgresivo = 0
    clasificacionTweets= []
    for tweet in tweepy.Cursor(api.search, q=keySearch, tweet_mode="extended", include_entitities = "false",lang = "es",result_type="recent").items(cantidadTweet):
        resultado = pipe.predict([tweet.full_text])
        if(resultado[0]==1):
            resultado = "Agresivo"
        else:
            resultado = "No Agresivo"
        clasificacionTweets.append({"tweet":tweet.full_text, "clf_modelo": resultado})
        if (resultado == "Agresivo"):
            numeroAgresivos +=1
        else:
            numeroNoAgresivo +=1
            
    return {"numero_agresivos" : numeroAgresivos,"numero_noAgresivos" : numeroNoAgresivo, "listaTweetClasificados":clasificacionTweets}
   
# PREDICE MULTIPLE TEXTO
@app.post("/predecir_multiple_texto/",response_model=tweetsClasificados)
def predecir_multiple_texto(modeloEntrada: modeloMultiplesTexto):
    if(modeloEntrada.clasificador == '-'):
        pipe = load('Modelos_entrenados/'+modeloEntrada.modelo+'_'+modeloEntrada.corpus+'.joblib')
    else:
        pipe = load('Modelos_entrenados/'+modeloEntrada.modelo+'_'+modeloEntrada.clasificador+'_'+modeloEntrada.corpus+'.joblib')
    clasificacionTexto = []
    numeroAgresivos = 0
    numeroNoAgresivo = 0
    for texto in modeloEntrada.listaTexto:
        clasificacion = pipe.predict([texto])
        if(clasificacion[0]==1):
            clasificacion = "Agresivo"
        else:
            clasificacion = "No Agresivo"
        clasificacionTexto.append({"tweet":texto, "clf_modelo": clasificacion})
        if (clasificacion == "Agresivo"):
            numeroAgresivos +=1
        else:
            numeroNoAgresivo +=1
    return {"numero_agresivos" : numeroAgresivos,"numero_noAgresivos" : numeroNoAgresivo, "listaTweetClasificados":clasificacionTexto}

# INSERTA UN MODELO EN LA BD
@app.post("/insertar_modelo")
def insertar_modelo(details: insertarModelo, db: Session = Depends(get_db)):
    to_create = modelos(
        id_modelo = details.id_modelo,
        enfoque = details.enfoque,
        algoritmo_ml = details.algoritmo_ml,
        corpus = details.corpus,
        f_measure = details.f_measure,
        accuracy = details.accuracy,
        precision = details.precision,
        recall = details.recall,
        descripcion = details.descripcion
    )
    db.add(to_create)
    db.commit()
    return {
        "successs": True,
        "created_id": to_create.id_modelo
    }

# INSERTA UN TWEET CLASIFICADO EN LA BD
@app.post("/insertar_tweet")
def insertar_tweet(details: insertarTweet, db: Session = Depends(get_db)):
    to_create = tweets_clfs(
        tweet = details.tweet,
        clf_modelo = details.clf_modelo,
        clf_humano = details.clf_humano, 
        id_modelo = details.id_modelo
    )
    db.add(to_create)
    db.commit()
    return { 
        "success": True,
        "created_id": to_create.id_tweet
    }

# INSERTA MUCHOS TWEETS CLASIFICADOS EN BD
@app.post("/insertar_tweets")
def insert_tweets(details: List[insertarTweet], db: Session = Depends(get_db)):
    
    for detail in details:    
        to_create = tweets_clfs(
            tweet = detail.tweet,
            clf_modelo = detail.clf_modelo,
            clf_humano = detail.clf_humano, 
            id_modelo = detail.id_modelo
        )
        db.add(to_create)
        db.commit()
    return { 
        "success": True
    }

# RETORNA TODOS LOS MODELOS GUARDADOS EN LA BD
@app.get("/obtener_modelos")
def get_all_modelos(db: Session = Depends(get_db)):
    return db.query(modelos).all()

# RETORNA TODOS LOS TWEETS GUARDADOS EN LA BD
@app.get("/obtener_tweets")
def get_all_tweets(db: Session = Depends(get_db)):
    return db.query(tweets_clfs).all()

# RETORNA EL CORPUS (TRAIN+TEST) EN UN .ZIP
@app.get("/descargar_corpus/{nombreCorpus}")
async def descargar_corpus(nombreCorpus: str):
    return FileResponse("corpusExperimentos/"+nombreCorpus+".zip")

# RETORNA METRICAS DEL MODELO SELECCIONADO SOBRE UN CORPUS DE PRUEBA 
@app.post("/evaluarmodelo/")
async def evaluar_modelo(file: UploadFile = File(...),enfoque: str = Form(...), algoritmo: str = Form(...) ,corpus: str = Form(...)):
    corpusTest = pd.read_csv(file.file)
    X_frases = np.asarray(corpusTest[['Texto']])
    X_frases = X_frases.ravel()
    Y_sentimientos = np.asarray(corpusTest[['Clasificacion']])
    Y_sentimientos = Y_sentimientos.ravel()
    if(algoritmo == '-'):
        pipe = load('Modelos_entrenados/'+enfoque+'_'+corpus+'.joblib')
    else:
        pipe = load('Modelos_entrenados/'+enfoque+'_'+algoritmo+'_'+corpus+'.joblib')

    Y_pred = pipe.predict(X_frases)
    return {"F1Weighted": f1_score(Y_sentimientos, Y_pred, average='weighted'),
            "Accuracy": accuracy_score(Y_sentimientos, Y_pred),
            "PrecisionWeighted": precision_score(Y_sentimientos, Y_pred,average='weighted'),
            "RecallWeighted":recall_score(Y_sentimientos, Y_pred,average='weighted')}

# RETORNA LA PLANTILLA SEGUN SU NOMBRE
@app.get("/descargar_plantilla/{nombrePlantilla}")
async def descargar_plantilla(nombrePlantilla: str):
    return FileResponse("Plantillas/"+nombrePlantilla+".csv")