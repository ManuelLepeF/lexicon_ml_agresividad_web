import re, os, nltk, spacy
from string import punctuation
from nltk.stem import SnowballStemmer

# Modifica la ruta de ejecucion para permitir la lectura de los archivo
rutaOriginal = os.getcwd()
os.chdir(os.path.abspath(__file__).replace(os.path.basename(__file__),""))

# Carga modelo spacy español
nlp = spacy.load("es_core_news_sm", disable=['ner', 'parser', 'tagger'])

# Archivos que contiene una lista de Stop Word
with open('archivos/stopwords.txt') as archivo_stopword:
    listaStopword=[line.rstrip('\n') for line in archivo_stopword]

# Se vuelve a la ruta original (para no afectar la ejecución de donde importa)
os.chdir(rutaOriginal)

# Lista que se usa para eliminar los signos
no_palabras = list(punctuation) 
no_palabras.extend(['¿', '¡'])

#  FUNCIONES PARA TOKENIZAR
# Solo tokenizar el doc usando spacy.
def Tokenizer(doc):
    doc = re.sub('[%s]' % re.escape(punctuation), ' ', doc)
    return [x.orth_ for x in nlp(doc.lower()) if (x.orth_ not in no_palabras)]

# Tokenizar y remover las stopwords del doc
def Tokenizer_con_stopwords(doc):
    doc = re.sub('[%s]' % re.escape(punctuation), ' ', doc)
    return [x.orth_ for x in nlp(doc.lower()) if (x.orth_ not in no_palabras and x.orth_ not in listaStopword)]

# Tokenizar y lematizar.
def Tokenizer_con_lemmatization(doc):
    doc = re.sub('[%s]' % re.escape(punctuation), ' ', doc)
    return [x.lemma_ for x in nlp(doc.lower()) if (x.orth_ not in no_palabras)]

# Tokenizar y hacer stemming con Snowball.
def Tokenizer_con_stemming(doc):
    doc = re.sub('[%s]' % re.escape(punctuation), ' ', doc)
    stemmer = SnowballStemmer('spanish')
    return [stemmer.stem(word) for word in [x.orth_ for x in nlp(doc.lower()) if (x.orth_ not in no_palabras)]]

# Tokenizar, remover stopword y hacer stemming con Porter
def Tokenizer_stopword_stemming(doc):
    doc = re.sub('[%s]' % re.escape(punctuation), ' ', doc)
    return [nltk.PorterStemmer().stem(word) for word in [x.orth_ for x in nlp(doc.lower()) if (x.orth_ not in no_palabras and x.orth_ not in listaStopword)]]

# Tokenizar con NLTK, remover stopword y hacer stemming con Porter
def TokenizeNLTK_stopword_stemming(text):
        text = re.sub('[%s]' % re.escape(punctuation), ' ', text)
        tokens = nltk.word_tokenize(text,  language='spanish')
        stems = []
        for item  in tokens:
            if (item not in no_palabras and item not in listaStopword):
                stems.append(nltk.PorterStemmer().stem(item))
        return stems